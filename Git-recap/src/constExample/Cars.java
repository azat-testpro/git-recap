package constExample;

public class Cars {

    String carModel;
    String carYear = "Unknown";

    //Constructor 1
    public Cars(String constructorCarModel) {
        this.carModel = constructorCarModel;
    }

    //Constructor 2
    public Cars(String constructorCarModel, String constructorCarYear) {
        this.carModel = constructorCarModel;
        this.carYear = constructorCarYear;
    }

    //Constructor 3
    public Cars() {

    }


    public void setCarModel(String givenCarModel) {
        this.carModel = givenCarModel;
    }


    public void printCarsBrand () {
        System.out.println("Car model is " + carModel + " Car year is " + carYear);
    }
}
