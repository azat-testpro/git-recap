package constExample;

public class StartingClass {

    public static void main(String[] args) {
        Cars bmwCar = new Cars("BMW", "2019");

        Cars mercedesCar = new Cars("Mercedes");

        Cars audiCar = new Cars();

        bmwCar.printCarsBrand();
        audiCar.printCarsBrand();
    }

}
