package example2;

import example.SecondExample;

public class Example2 {

    public static void main(String[] args) {
        SecondExample objectExample = new SecondExample();
//        objectExample.protectedMethod();
//        objectExample.privatePrint();
        objectExample.publicPrint();
    }
}
