package example;

public class SecondExample {

    protected void protectedMethod() {
        System.out.println("Print Me Message");
    }

    private void privatePrint () {
        System.out.println("Print Private Message");
    }

    public void publicPrint () {
        System.out.println("Print Public Message");
    }

}
