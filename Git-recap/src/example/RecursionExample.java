package example;

public class RecursionExample {

    // print numbers from 1 to 10 in the console.

    static int i = 0;

    public RecursionExample() {}

    public static void main(String[] args) {



        recursion();

    }

    public static void recursion() {
        i++;
        if (i <= 10) {
            if (i!=7){
                System.out.println(i);
            }
            recursion();
        }
    }
}
